package com.innovenso.townplan.gradle;

import com.innovenso.townplan.writer.TownPlanWriter;
import com.innovenso.townplan.writer.confluence.*;
import com.innovenso.townplan.writer.svg.TownPlanSvgWriter;
import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TownPlanConfluenceExporter extends AbstractTownPlanExporter {
	protected final TownPlanConfluenceWriter confluenceWriter;
	protected final TownPlanConfluenceNavigationService navigationService;
	protected final TownPlanConfluenceAttachmentService attachmentService;
	protected final TownPlanConfluencePageService pageService;
	protected final TownPlanSvgWriter svgWriter;

	public TownPlanConfluenceExporter(@NonNull String... args) throws IllegalArgumentException {
		super(args);
		final ConfluenceGateway confluenceGateway = buildConfluenceGateway();
		final ConfluenceSpaceConfig spaceConfig = buildSpaceConfig();

		this.attachmentService = new TownPlanConfluenceAttachmentService(outputPath.toString(), outputUri,
				confluenceGateway);
		this.navigationService = new TownPlanConfluenceNavigationService(confluenceGateway, spaceConfig);
		this.pageService = new TownPlanConfluencePageService(confluenceGateway, spaceConfig, attachmentService,
				navigationService, buildSparxBaseUrl());
		this.confluenceWriter = new TownPlanConfluenceWriter(assetRepository, outputPath.toString(), pageService,
				navigationService);
		this.svgWriter = new TownPlanSvgWriter(buildPath.toString(), assetRepository);
	}

	private String buildSparxBaseUrl() {
		return Optional.ofNullable(System.getenv("TOWNPLAN_WRITER_CONFLUENCE_SPARX_URL")).orElse("");
	}

	private ConfluenceGateway buildConfluenceGateway() {
		final String confluenceApiUrl = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_URL");
		final String confluenceUser = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_USER");
		final String confluenceApiKey = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_APIKEY");
		return new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway();
	}

	private ConfluenceSpaceConfig buildSpaceConfig() {
		final List<String> confluenceSpaces = Arrays
				.asList(System.getenv("TOWNPLAN_WRITER_CONFLUENCE_SPACES").split(","));
		final String confluenceMainMenu = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME");
		final String confluencePostfix = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX");
		final String confluenceBaseUrl = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_BASEURL");
		return new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl);
	}

	@Override
	protected List<TownPlanWriter> getTownPlanWriters() {
		return List.of(svgWriter, confluenceWriter);
	}

}
