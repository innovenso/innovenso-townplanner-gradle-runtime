package com.innovenso.townplan.gradle;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.dsl.TownPlanFactory;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.repository.FileSystemAssetRepository;
import com.innovenso.townplan.writer.TownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Log4j2
public abstract class AbstractTownPlanExporter {
	protected final Path outputPath;
	protected final Path buildPath;
	protected final String outputUri;
	protected final AssetRepository assetRepository;
	protected final TownPlanFactory factory;

	public AbstractTownPlanExporter(@NonNull String... args) throws IllegalArgumentException {
		this.outputPath = Paths.get("").toAbsolutePath().resolve("output");
		this.buildPath = Paths.get("").toAbsolutePath().resolve("build");
		this.outputUri = buildOutputUri(outputPath);
		this.assetRepository = new FileSystemAssetRepository(outputPath.toString(), outputUri);
		final String factoryClassName = getFactoryClassName(args);
		try {
			this.factory = (TownPlanFactory) Class.forName(factoryClassName).getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException("factory of type " + factoryClassName + " cannot be instantiated.", e);
		}
	}

	private String buildOutputUri(@NonNull Path outputPath) {
		final String outputUri = outputPath.toUri().toString();
		return outputUri.endsWith("/") ? outputUri : outputUri + "/";
	}

	public void run(@NonNull String... args) {
		getConceptKey(args).ifPresentOrElse(this::writeConcept, this::writeFullTownPlan);
		// this application is always run in a separate child process, in a dedicated
		// JVM.
		// since some IO writers tend to start background threads (PlantUML and Blender
		// for example)
		// explicitly exit here after the run is complete.
		System.exit(0);
	}

	protected abstract List<TownPlanWriter> getTownPlanWriters();

	private void writeConcept(@NonNull String conceptKey) {
		final TownPlan townPlan = this.factory.getTownPlan();
		getTownPlanWriters().forEach(writer -> writer.write(townPlan, conceptKey));
	}

	private void writeFullTownPlan() {
		final TownPlan townPlan = this.factory.getTownPlan();
		getTownPlanWriters().forEach(writer -> writer.write(townPlan));
	}

	private String getFactoryClassName(String... args) {
		if (args.length > 0)
			return args[0];
		else
			throw new IllegalArgumentException("Town Plan factory class name must be specified");
	}

	private Optional<String> getConceptKey(String... args) {
		if (args.length > 1)
			return Optional.of(args[1]);
		else
			return Optional.empty();
	}

}
