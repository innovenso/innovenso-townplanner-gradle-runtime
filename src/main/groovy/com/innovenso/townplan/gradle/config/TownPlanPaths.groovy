package com.innovenso.townplan.gradle.config

import lombok.NonNull
import org.springframework.stereotype.Component

import java.nio.file.Path
import java.nio.file.Paths

@Component
class TownPlanPaths {
	final Path outputPath;
	final Path buildPath;
	final String outputUri;

	public TownPlanPaths() {
		this.outputPath = Paths.get("").toAbsolutePath().resolve("output");
		this.buildPath = Paths.get("").toAbsolutePath().resolve("build");
		this.outputUri = buildOutputUri(outputPath);
	}

	private String buildOutputUri(@NonNull Path outputPath) {
		final String outputUri = outputPath.toUri().toString();
		return outputUri.endsWith("/") ? outputUri : outputUri + "/";
	}

	public String getOutputPathAsString() {
		return outputPath.toString();
	}

	public String getBuildPathAsString() {
		return buildPath.toString();
	}
}
