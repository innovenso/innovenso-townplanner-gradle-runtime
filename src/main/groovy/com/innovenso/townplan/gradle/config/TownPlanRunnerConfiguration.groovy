package com.innovenso.townplan.gradle.config

import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.TownPlanWriter
import com.innovenso.townplan.writer.archimate.TownPlanOpenGroupExchangeWriter
import com.innovenso.townplan.writer.blender.TownPlanBlenderWriter
import com.innovenso.townplan.writer.confluence.ConfluenceGateway
import com.innovenso.townplan.writer.confluence.ConfluenceGatewayFactory
import com.innovenso.townplan.writer.confluence.ConfluenceSpaceConfig
import com.innovenso.townplan.writer.confluence.TownPlanConfluenceAttachmentService
import com.innovenso.townplan.writer.confluence.TownPlanConfluenceNavigationService
import com.innovenso.townplan.writer.confluence.TownPlanConfluencePageService
import com.innovenso.townplan.writer.confluence.TownPlanConfluenceWriter
import com.innovenso.townplan.writer.excel.TownPlanExcelWriter
import com.innovenso.townplan.writer.latex.TownPlanLatexWriter
import com.innovenso.townplan.writer.svg.TownPlanSvgWriter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
class TownPlanRunnerConfiguration {
	@Bean
	public AssetRepository assetRepository(TownPlanPaths paths) {
		return new FileSystemAssetRepository(paths.getOutputPathAsString(), paths.getOutputUri());
	}

	@Bean
	@Profile(["svg", "confluence"])
	public TownPlanWriter svgWriter(TownPlanPaths paths, AssetRepository repository) {
		return new TownPlanSvgWriter(paths.buildPathAsString, repository)
	}

	@Bean
	@Profile(["archimate", "confluence"])
	public TownPlanWriter archimateWriter(TownPlanPaths paths, AssetRepository repository) {
		return new TownPlanOpenGroupExchangeWriter(repository, paths.buildPathAsString)
	}

	@Bean
	@Profile("blender")
	public TownPlanWriter blenderWriter(TownPlanPaths paths, AssetRepository repository) {
		return new TownPlanBlenderWriter(paths.buildPathAsString, repository)
	}

	@Bean
	@Profile(["excel", "confluence"])
	public TownPlanWriter excelWriter(TownPlanPaths paths, AssetRepository repository) {
		return new TownPlanExcelWriter(repository, paths.buildPathAsString)
	}

	@Bean
	@Profile(["latex", "confluence"])
	public TownPlanWriter latexWriter(TownPlanPaths paths, AssetRepository repository) {
		final String theme = Optional.ofNullable(System.getenv("TOWNPLAN_WRITER_LATEX_THEME")).orElse("innovenso");
		final String institution = Optional.ofNullable(System.getenv("TOWNPLAN_WRITER_LATEX_INSTITUTION"))
				.orElse("Innovenso");
		return new TownPlanLatexWriter(paths.buildPathAsString, theme, institution, repository);
	}

	@Bean
	@Profile("confluence")
	public ConfluenceGateway confluenceGateway() {
		final String confluenceApiUrl = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_URL");
		final String confluenceUser = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_USER");
		final String confluenceApiKey = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_APIKEY");
		return new ConfluenceGatewayFactory(confluenceApiUrl, confluenceUser, confluenceApiKey).confluenceGateway();
	}

	@Bean
	@Profile("confluence")
	public ConfluenceSpaceConfig spaceConfig() {
		final List<String> confluenceSpaces = Arrays
				.asList(System.getenv("TOWNPLAN_WRITER_CONFLUENCE_SPACES").split(","));
		final String confluenceMainMenu = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME");
		final String confluencePostfix = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX");
		final String confluenceBaseUrl = System.getenv("TOWNPLAN_WRITER_CONFLUENCE_BASEURL");
		return new ConfluenceSpaceConfig(confluenceSpaces, confluenceMainMenu, confluencePostfix, confluenceBaseUrl);
	}

	private String sparxBaseUrl() {
		return Optional.ofNullable(System.getenv("TOWNPLAN_WRITER_CONFLUENCE_SPARX_URL")).orElse("");
	}

	@Bean
	@Profile("confluence")
	public TownPlanConfluenceAttachmentService attachmentService(TownPlanPaths paths, ConfluenceGateway gateway) {
		return new TownPlanConfluenceAttachmentService(paths.outputPathAsString, paths.outputUri, gateway)
	}

	@Bean
	@Profile("confluence")
	public TownPlanConfluenceNavigationService navigationService(ConfluenceGateway gateway, ConfluenceSpaceConfig spaceConfig) {
		return new TownPlanConfluenceNavigationService(gateway, spaceConfig)
	}

	@Bean
	@Profile("confluence")
	public TownPlanConfluencePageService pageService(ConfluenceGateway confluenceGateway, ConfluenceSpaceConfig spaceConfig, TownPlanConfluenceAttachmentService attachmentService, TownPlanConfluenceNavigationService navigationService) {
		new TownPlanConfluencePageService(confluenceGateway, spaceConfig, attachmentService,
				navigationService, sparxBaseUrl())
	}

	@Bean
	@Profile("confluence")
	public TownPlanConfluenceWriter confluenceWriter(AssetRepository assetRepository, TownPlanPaths paths,
			TownPlanConfluencePageService pageService, TownPlanConfluenceNavigationService navigationService,
			TownPlanSvgWriter svgWriter, TownPlanExcelWriter excelWriter, TownPlanOpenGroupExchangeWriter archimateWriter,
			TownPlanLatexWriter latexWriter
	) {
		new TownPlanConfluenceWriter(assetRepository, paths.outputPathAsString, pageService,
				navigationService)
	}
}
