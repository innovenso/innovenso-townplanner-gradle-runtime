package com.innovenso.townplan.gradle

import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import com.innovenso.townplan.writer.TownPlanWriter
import com.innovenso.townplan.writer.svg.TownPlanSvgWriter
import lombok.NonNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication(scanBasePackages = ["townplan", "com.innovenso.townplan.gradle"])
class TownPlanRunner implements CommandLineRunner {
	private final TownPlanWriterCollection writers

	@Autowired
	TownPlanRunner(@NonNull final TownPlanWriterCollection writers) {
		this.writers = writers
	}

	public static void main(String... args) {
		SpringApplication.run(TownPlanRunner.class, args);
	}

	@Override
	void run(String... args) throws Exception {
		println 'Running Town Planner'
		writers.write(args)
		// this application is always run in a separate child process, in a dedicated
		// JVM.
		// since some IO writers tend to start background threads (PlantUML and Blender
		// for example)
		// explicitly exit here after the run is complete.
		System.exit(0)
	}
}
