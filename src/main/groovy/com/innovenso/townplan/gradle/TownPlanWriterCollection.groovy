package com.innovenso.townplan.gradle

import com.innovenso.townplan.api.TownPlan
import com.innovenso.townplan.dsl.TownPlanFactory
import com.innovenso.townplan.writer.TownPlanWriter
import lombok.NonNull
import org.springframework.stereotype.Component

@Component
class TownPlanWriterCollection {
	private final List<TownPlanWriter> writers = new ArrayList<>()
	private final TownPlanFactory townPlanFactory

	TownPlanWriterCollection(TownPlanFactory factory) {
		this.townPlanFactory = factory
	}

	public void addWriter(TownPlanWriter writer) {
		this.writers.add(writer)
	}

	public void write(String... args) {
		if (getConceptKey(args).isPresent()) {
			this.writeConcept(getConceptKey(args).get())
		} else {
			writeFullTownPlan()
		}
	}

	private void writeConcept(@NonNull String conceptKey) {
		final TownPlan townPlan = this.townPlanFactory.getTownPlan()
		writers.each {
			it.write(townPlan, conceptKey)
		}
	}

	private void writeFullTownPlan() {
		final TownPlan townPlan = this.townPlanFactory.getTownPlan()
		writers.each { it.write(townPlan)}
	}

	private static Optional<String> getConceptKey(String... args) {
		String conceptKey = args.find {it.startsWith("--concept=") }
		if (conceptKey)
			return Optional.of(conceptKey - '--concept=');
		else
			return Optional.empty();
	}
}
