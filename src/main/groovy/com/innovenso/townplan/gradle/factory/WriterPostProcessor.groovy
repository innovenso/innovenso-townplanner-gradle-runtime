package com.innovenso.townplan.gradle.factory

import com.innovenso.townplan.gradle.TownPlanWriterCollection
import com.innovenso.townplan.writer.TownPlanWriter
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.stereotype.Component

@Component
class WriterPostProcessor implements BeanPostProcessor {
	private final TownPlanWriterCollection writerCollection

	WriterPostProcessor(TownPlanWriterCollection writerCollection) {
		this.writerCollection = writerCollection
	}

	@Override
	Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof TownPlanWriter) {
			println "adding writer ${beanName}"
			TownPlanWriter writer = (TownPlanWriter)bean
			writerCollection.addWriter writer
		}
		return bean
	}
}
