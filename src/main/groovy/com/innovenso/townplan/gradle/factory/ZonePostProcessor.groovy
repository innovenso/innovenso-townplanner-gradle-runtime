package com.innovenso.townplan.gradle.factory

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.gradle.factory.ComponentScanTownPlanFactory
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.stereotype.Component

@Component
class ZonePostProcessor implements BeanPostProcessor {
	private final ComponentScanTownPlanFactory factory

	ZonePostProcessor(ComponentScanTownPlanFactory factory) {
		this.factory = factory
	}

	@Override
	Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof Zone) {
			println "adding zone ${beanName}"
			Zone zone = (Zone)bean
			factory.townPlanConfigurer.zone zone
		}
		return bean
	}
}
