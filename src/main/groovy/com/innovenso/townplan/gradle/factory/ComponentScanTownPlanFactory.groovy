package com.innovenso.townplan.gradle.factory

import com.innovenso.townplan.api.TownPlan
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.TownPlanFactory
import org.springframework.stereotype.Component

@Component
class ComponentScanTownPlanFactory implements TownPlanFactory {
	final TownPlanConfigurer townPlanConfigurer

	ComponentScanTownPlanFactory() {
		this.townPlanConfigurer = new TownPlanConfigurer()
	}

	@Override
	public TownPlan getTownPlan() {
		townPlanConfigurer.apply()
	}
}
