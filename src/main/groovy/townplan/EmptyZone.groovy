package townplan

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.gradle.IncludeInTownplan
import org.springframework.stereotype.Component

@IncludeInTownplan
class EmptyZone extends Zone {
	EmptyZone() {
		concepts {
			enterprise {
				title "Innovenso"
				description "Enterprise Architecture As Code"
			}
		}
	}
}
