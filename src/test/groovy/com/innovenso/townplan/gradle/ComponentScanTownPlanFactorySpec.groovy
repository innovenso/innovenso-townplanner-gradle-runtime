package com.innovenso.townplan.gradle

import com.innovenso.townplan.api.TownPlan
import com.innovenso.townplan.api.value.business.Enterprise
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock
import com.innovenso.townplan.api.value.strategy.BusinessCapability
import com.innovenso.townplan.dsl.TownPlanFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
@EnableConfigurationProperties
class ComponentScanTownPlanFactorySpec extends Specification {
	@Autowired TownPlanFactory factory

	def 'zones are loaded as Spring components and added to the town plan'() {
		when:
		TownPlan townPlan = factory.getTownPlan()
		then:
		townPlan.getElements(Enterprise).size() == 1
		townPlan.getElements(BusinessCapability).size() == 1
		townPlan.getElements(ArchitectureBuildingBlock).size() == 1
		townPlan.getAllRelationships().size() == 1
	}
}
