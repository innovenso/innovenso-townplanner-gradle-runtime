package townplan

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer as Enterprise
import com.innovenso.townplan.gradle.IncludeInTownplan

@IncludeInTownplan
class EnterpriseZone extends Zone {
	Enterprise innovenso

	EnterpriseZone() {
		concepts {
			this.innovenso = enterprise {
				title "Innovenso BV"
				description "Enterprise Architecture as Code"
			}
		}
	}
}
