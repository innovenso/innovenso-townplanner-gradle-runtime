package townplan

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.gradle.IncludeInTownplan
import org.springframework.stereotype.Component

@IncludeInTownplan
class BuildingBlockZone extends Zone {
	BuildingBlockZone(EnterpriseZone enterpriseZone, CapabilityZone capabilityZone) {
		concepts {
			buildingBlock {
				title "a building block"
				enterprise enterpriseZone.innovenso

				realizes capabilityZone.aCapability
			}
		}
	}
}
