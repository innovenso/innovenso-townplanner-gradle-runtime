package townplan

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import com.innovenso.townplan.gradle.IncludeInTownplan
import lombok.NonNull
import org.springframework.stereotype.Component


@IncludeInTownplan
class CapabilityZone extends Zone {
	BusinessCapabilityConfigurer aCapability

	CapabilityZone(@NonNull EnterpriseZone enterpriseZone) {
		concepts {
			this.aCapability = capability {
				title "A capability"
				enterprise enterpriseZone.innovenso
			}
		}
	}
}
